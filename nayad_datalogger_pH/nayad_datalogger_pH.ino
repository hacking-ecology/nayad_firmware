/***********************************************************************
  # This code is developed to be used with Nayad Modular pH version
  # Editor : smsj
  # Version: 1.1
  # Date   : 2021.06.27
  # Sensor: pH

            GNU General Public License version 3.
  See <https://www.gnu.org/licenses/gpl-3.0.en.html> for details.
  All above must be included in any redistribution
************************************************************************/

////////////////////////////////////////////////////////////////////////
////////////////////// Calibration directions /////////////////////////
///////////////////////////////////////////////////////////////////////
/************************************************************************
At the SERIAL SCREEN:

  enterph -> enter the calibration mode
  calph   -> calibrate with the standard buffer solution (4.0 and 7.0). 
  They will be automatically recognized.
  exitph  -> save the calibrated parameters and exit from calibration mode
************************************************************************/

#include <ArduinoJson.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include "NAYAD_PH.h"
#include <NAYAD_RTC.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ThingsBoard.h>
#include <Wire.h>
#include <WiFi.h>
#include <OneWire.h>

///////////////////////////////////////////////////////////////////
///////////////////// Firmware Configuration///////////////////////
///////////////////////////////////////////////////////////////////

// CLOUD SETUP
#define WIFI_AP "vodafoneB9E8"
#define WIFI_PASSWORD "2dWPgwtT@$"
#define TOKEN "5IWCsJrAhai9K1lrXCc6"
char NayadServer[] = "sensing.hackingecology.com";

//WiFi CONFIG
WiFiClient wifiClient;
ThingsBoard sensing(wifiClient);
int status = WL_IDLE_STATUS;
unsigned long lastSend;

//TOUCH CONFIG
int threshold = 44; // check the threshold value using the "touchread" code (at examples)
bool touch1detected = false;

// SD CONFIG
File myFile;

// RTC CONFIG
NAYAD_RTC RTC;
DateTime timeRTC = DateTime(__DATE__, __TIME__);

// TEMPERATURE CONFIG
const int oneWireBus1 = 0;
OneWire oneWire1 (oneWireBus1);
DallasTemperature sensorsTemp (&oneWire1);

// pH Sensor
NAYAD_PH ph;
#define ESPADC 4096.0   //the esp Analog Digital Conversion value
#define ESPVOLTAGE 3300 //the esp voltage supply value
#define PH_PIN 33       //the esp GPIO data pin number (NAYAD uses pin 33)
float voltage, phValue;


///////////////////////////////////////////////////////////////////
///////////////////// Main functions///////////////////////////////
///////////////////////////////////////////////////////////////////

void setup() {
  Serial.begin(115200);
  InitWiFi();
  lastSend = 0;

  //SD card setup
  touchAttachInterrupt(T0, gotTouch1, threshold);

  if (!SD.begin()) {
    Serial.println("Card Mount Failed");
    return;
  }
  else {
    Serial.println("Card is Ready");
    return;
  }

  //RTC setup

  DateTime compiled = DateTime(__DATE__, __TIME__);

  Serial.println("Checking I2C device...");
  if (RTC.searchDevice())
  {
    Serial.println("configuring RTC I2C");
    RTC.configure();

    if (!RTC.IsDateTimeValid())
    {
      if (RTC.LastError() != 0)
      {
        Serial.print("RTC communications error = ");
        Serial.println(RTC.LastError());
      }
      else
      {
        Serial.println("RTC lost confidence in the DateTime!");

        RTC.SetDateTime(compiled);
      }
    }
    else
    {
      Serial.printf("Found an RTC with valid time\n");
    }

    timeRTC = RTC.now();
    uint32_t nowTS = timeRTC.getTimeStamp();
    uint32_t compiledTS = compiled.getTimeStamp();
    if (nowTS < compiledTS)
    {
      Serial.printf("RTC is older than compile time!  (Updating DateTime)\n");
      RTC.SetDateTime(compiled);
    }
    else if (nowTS > compiledTS)
    {
      Serial.printf("RTC is newer than compile time. (this is expected)\n");
    }
    else if (nowTS == compiledTS)
    {
      Serial.printf("RTC is the same as compile time! (not expected but all is fine)\n");
    }

    if (!timeRTC.checkWeek())
    {
      Serial.printf("Update WEEK\n");
      RTC.setWeekDays(dow(timeRTC.year(), timeRTC.month(), timeRTC.day()));
    }
  }
  else
  {
    Serial.printf("device not found\n");
    while (1);
  }

  // pH Setup
  EEPROM.begin(35);//allows storage of calibration value in eeprom (always ADC)
  ph.begin();


}

void loop() {

  //Loop for WIFI
  if ( !sensing.connected() ) {
    reconnect();
  }

  sensing.loop();

// Loop pH
  SensorsData();
  delay(1000);
}

///////////////////////////////////////////////////////////////////
///////////////////// UTILITY FUNCTION ////////////////////////////
///////////////////////////////////////////////////////////////////

//Touch
void gotTouch1() {
  touch1detected = true;
}

//SD card
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}


//WIFI
void InitWiFi() {
  Serial.println("Connecting to AP ...");
  // attempt to connect to WiFi network

  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}


void reconnect() {
  // Loop until we're connected
  while (!sensing.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(WIFI_AP, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to Sensing Platform ...");
    if ( sensing.connect(NayadServer, TOKEN) ) {
      Serial.println( "[DONE]" );
    } else {
      Serial.print( "[FAILED]" );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}


// pH and Temperature sensor

void SensorsData() {
  sensorsTemp.requestTemperatures();
  float temperature = sensorsTemp.getTempCByIndex(0);

  {
    static unsigned long timepoint = millis();
    if (millis() - timepoint > 1000U) //time interval: 1 second
    {
      timepoint = millis();
      voltage = analogRead(PH_PIN) / ESPADC * ESPVOLTAGE; // read the voltage
      Serial.print("voltage:");
      Serial.println(voltage, 4);

      //temperature = readTemperature();  // Remove the comment here to use data from a temperature sensor
      Serial.print("temperature:");
      Serial.print(temperature, 1);
      Serial.println("^C");

      phValue = ph.readPH(voltage, temperature); // convert voltage to pH with temperature compensation
      Serial.print("pH:");
      Serial.println(phValue, 4);

//Loop for RTC
      timeRTC = RTC.now();
    
      if (timeRTC.IsValid())
      {
        Serial.printf("str Data: %s\n", timeRTC.getStrDate().c_str());
        Serial.printf("str Hora: %s\n", timeRTC.getStrTime().c_str());
        Serial.printf("TS: %u\n", timeRTC.getTimeStamp());
        delay(random(1000, 5000));
          }
      else
      {
        Serial.printf("Invalid DateTime\n");
      }
    }
    ph.calibration(voltage, temperature); // calibration process by Serial CMD

    

    sensing.sendTelemetryFloat("pH", phValue);
    sensing.sendTelemetryFloat("temperature", temperature);
    delay( 1000 );

// ######## setup touch and SD card Storage
 
    if (touch1detected) {
    touch1detected = false;
    Serial.println("Touch 1 detected");

      if (SD.exists("/data.csv")) {
        Serial.println("Appending line...");
        File file = SD.open("/data.csv", FILE_APPEND);
        if (!file) {
          Serial.println("Failed to open file for appending");
          return;
        }
        if (file) {
          file.print(timeRTC.getStrDate().c_str());
          file.print(" ");
          file.print(timeRTC.getStrTime().c_str());
          file.print(",");
          file.print(timeRTC.getTimeStamp());
          file.print(",");    
          file.print(temperature);
          file.print(",");
          file.println(phValue, 2);
          delay(1000);          
          Serial.println("Message appended");
        } else {
          Serial.println("Append failed");
        }
        file.close();
      }
      else if (!SD.exists("/data.csv")) {
        File file = SD.open("/data.csv", FILE_WRITE);
        if (!file) {
          Serial.println("Failed to open file for writing");
          return;
        }
        if (file) {
          file.println("Time, UNIX, Temperature (ºC), pH");
          file.print(timeRTC.getStrDate().c_str());
          file.print(" ");
          file.print(timeRTC.getStrTime().c_str());
          file.print(",");
          file.print(timeRTC.getTimeStamp());
          file.print(",");    
          file.print(temperature);
          file.print(",");
          file.println(phValue, 2);
          Serial.println("File written");
        } else {
          Serial.println("Write failed");
        }
        file.close();
      }
    }
  }
}
