# Nayad Firmware

Repo with files to test and run the Nayad Modular Board

En tools > libraries manager:

```
ArduinoHttpClient
ArduinoJson
DallasTemperature
OneWire
PubSubClient
ThingsBoard
time // pre-installed with ESP32
FS // pre-isntalled with ESP32
SD
SPI

```

External library:

- pH (NAYAD_pH)

https://gitlab.com/hacking-ecology/NAYAD_pH

- Dissolved Oxygen (nayad_do)

https://gitlab.com/hacking-ecology/nayad_do
