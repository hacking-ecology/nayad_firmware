/***************************************************
  GNU Lesser General Public License.
  See <http://www.gnu.org/licenses/> for details.
 ****************************************************/

/***********Notice and Trouble shooting***************
  1. This code is tested on Arduino Uno with Arduino IDE 1.0.5 r2 and 1.8.2.
  2. Calibration CMD:
    enter -> enter the calibration mode
    cal:tds value -> calibrate with the known tds value(25^c). e.g.cal:707
    exit -> save the parameters and exit the calibration mode
****************************************************/
#include <Arduino.h>
#include <EEPROM.h>
#include "GravityTDS.h"
#include <WiFi.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include <NAYAD_RTC.h>
#include <pgmspace.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <ThingsBoard.h>
#include <Wire.h>
#include <WiFi.h>
#include <OneWire.h>


///////////////////////////////////////////////////////////////////
///////////////////// Firmware Configuration///////////////////////
///////////////////////////////////////////////////////////////////
// CLOUD SETUP
#define WIFI_AP "vodafoneB9E8"
#define WIFI_PASSWORD "2dWPgwtT@$"
#define TOKEN "5IWCsJrAhai9K1lrXCc6"
char NayadServer[] = "sensing.hackingecology.com";

//WiFi CONFIG
WiFiClient wifiClient;
ThingsBoard sensing(wifiClient);
int status = WL_IDLE_STATUS;
unsigned long lastSend;

//TOUCH CONFIG
int threshold = 44; // check the threshold value using the "touchread" code (at examples)
bool touch1detected = false;

// SD CONFIG
File myFile;

// RTC CONFIG
NAYAD_RTC RTC;
DateTime timeRTC = DateTime(__DATE__, __TIME__);

// TEMPERATURE CONFIG
const int oneWireBus1 = 0;
OneWire oneWire1 (oneWireBus1);
DallasTemperature sensorsTemp (&oneWire1);
float temperature_sensor;

#define TdsSensorPin 33
GravityTDS gravityTds;

float temperature = 25, tdsValue = 0;

//Touch
void gotTouch1() {
  touch1detected = true;
}


void setup()
{
  Serial.begin(115200);

  InitWiFi();
  lastSend = 0;
  gravityTds.setPin(TdsSensorPin);
  gravityTds.setAref(3.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
  gravityTds.setAdcRange(4096);  //1024 for 10bit ADC;4096 for 12bit ADC
  gravityTds.begin();  //initialization


  sensorsTemp.begin();

  //SD card setup
  touchAttachInterrupt(T0, gotTouch1, threshold);

  if (!SD.begin()) {
    Serial.println("Card Mount Failed");
    return;
  }
  else {
    Serial.println("Card is Ready");
    return;
  }


  //RTC Setup
  DateTime compiled = DateTime(__DATE__, __TIME__);

  Serial.println("Checking I2C device...");
  if (RTC.searchDevice())
  {
    Serial.println("configuring RTC I2C");
    RTC.configure();

    if (!RTC.IsDateTimeValid())
    {
      if (RTC.LastError() != 0)
      {
        Serial.print("RTC communications error = ");
        Serial.println(RTC.LastError());
      }
      else
      {
        Serial.println("RTC lost confidence in the DateTime!");
        RTC.SetDateTime(compiled);
      }
    }
    else
    {
      Serial.printf("Found an RTC with valid time\n");
    }

    timeRTC = RTC.now();
    uint32_t nowTS = timeRTC.getTimeStamp();
    uint32_t compiledTS = compiled.getTimeStamp();
    if (nowTS < compiledTS)
    {
      Serial.printf("RTC is older than compile time!  (Updating DateTime)\n");
      RTC.SetDateTime(compiled);
    }
    else if (nowTS > compiledTS)
    {
      Serial.printf("RTC is newer than compile time. (this is expected)\n");
    }
    else if (nowTS == compiledTS)
    {
      Serial.printf("RTC is the same as compile time! (not expected but all is fine)\n");
    }

    if (!timeRTC.checkWeek())
    {
      Serial.printf("Update WEEK\n");
      RTC.setWeekDays(dow(timeRTC.year(), timeRTC.month(), timeRTC.day()));
    }
  }
  else
  {
    Serial.printf("device not found\n");
    while (1);
  }
}

void loop()
{
  
  //Loop for WIFI
  if ( !sensing.connected() ) {
    reconnect();
  }

  sensing.loop();

  SensorsTDS();

}


///////////////////////////////////////////////////////////////////
///////////////////// UTILITY FUNCTION ////////////////////////////
///////////////////////////////////////////////////////////////////

void SensorsTDS() {
  sensorsTemp.requestTemperatures();
  float temperature_sensor = sensorsTemp.getTempCByIndex(0);

  {
    static unsigned long timepoint = millis();
    if (millis() - timepoint > 1000U) //time interval: 1 second
    {
      //temperature = readTemperature();  //add your temperature sensor and read it
      gravityTds.setTemperature(temperature_sensor);  // set the temperature and execute temperature compensation
      gravityTds.update();  //sample and calculate
      tdsValue = gravityTds.getTdsValue();  // then get the value
      Serial.print(tdsValue, 0);
      Serial.println("ppm");
      delay(1000);

      Serial.print("temperature: ");
      Serial.print(temperature_sensor, 2);
      Serial.println(" ^C");
      Serial.print("TDS (mg/L: ");
      Serial.print(tdsValue);

      //Loop for RTC
      timeRTC = RTC.now();

      if (timeRTC.IsValid())
      {
        Serial.printf("str Data: %s\n", timeRTC.getStrDate().c_str());
        Serial.printf("str Hora: %s\n", timeRTC.getStrTime().c_str());
        Serial.printf("TS: %u\n", timeRTC.getTimeStamp());
        delay(random(1000, 5000));
      }
      else
      {
        Serial.printf("Invalid DateTime\n");
      }
    }
    sensing.sendTelemetryFloat("TDS", tdsValue);
    sensing.sendTelemetryFloat("temperature", temperature_sensor);
    delay( 1000 );

    // ######## setup touch and SD card Storage

    if (touch1detected) {
      touch1detected = false;
      Serial.println("Touch 1 detected");

      if (SD.exists("/data.csv")) {
        Serial.println("Appending line...");
        File file = SD.open("/data.csv", FILE_APPEND);
        if (!file) {
          Serial.println("Failed to open file for appending");
          return;
        }
        if (file) {
          file.print(timeRTC.getStrDate().c_str());
          file.print(" ");
          file.print(timeRTC.getStrTime().c_str());
          file.print(",");
          file.print(timeRTC.getTimeStamp());
          file.print(",");
          file.print(temperature_sensor);
          file.print(",");
          file.println(tdsValue);
          delay(1000);
          Serial.println("Message appended");
        } else {
          Serial.println("Append failed");
        }
        file.close();
      }
      else if (!SD.exists("/data.csv")) {
        File file = SD.open("/data.csv", FILE_WRITE);
        if (!file) {
          Serial.println("Failed to open file for writing");
          return;
        }
        if (file) {
          file.println("Time, UNIX, Temperature (ºC), TDS (mg/L)");
          file.print(timeRTC.getStrDate().c_str());
          file.print(" ");
          file.print(timeRTC.getStrTime().c_str());
          file.print(",");
          file.print(timeRTC.getTimeStamp());
          file.print(",");
          file.print(temperature_sensor);
          file.print(",");
          file.println(tdsValue);
          Serial.println("File written");
        } else {
          Serial.println("Write failed");
        }
        file.close();
      }
    }
  }
}


//SD card
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}


//WIFI
void InitWiFi() {
  Serial.println("Connecting to AP ...");
  // attempt to connect to WiFi network

  WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("Connected to AP");
}

void reconnect() {
  // Loop until we're connected
  while (!sensing.connected()) {
    status = WiFi.status();
    if ( status != WL_CONNECTED) {
      WiFi.begin(WIFI_AP, WIFI_PASSWORD);
      while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
      }
      Serial.println("Connected to AP");
    }
    Serial.print("Connecting to Sensing Platform ...");
    if ( sensing.connect(NayadServer, TOKEN) ) {
      Serial.println( "[DONE]" );
    } else {
      Serial.print( "[FAILED]" );
      Serial.println( " : retrying in 5 seconds]" );
      // Wait 5 seconds before retrying
      delay( 5000 );
    }
  }
}
